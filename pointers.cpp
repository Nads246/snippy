 /*  IMPORTANT NOTES:

   the & symbol:
       
    I) if a data type preceeds &, then the & means
       a reference variable is being declared

       Eg.
           int & x  // x is a reference variable
           char& y = ch; //y is a reference to ch
           auto& z  = y; // etc..
           Demo& d  //etc..
   
 
    II) If the & symbol is not preceeded by a data  
         type, then we are asking for the "address" 
         of the varible (that follows after it) 

         Eg.  &y; //address of variable y
              auto a = & z; // address of variable z
              cout << &v ; // etc ... 

*/	
		
	
/*
   the * symbol:
       
   I) if a data type preceeds *, then the * means
       a pointer  variable is being declared

       Eg.
           int * x; // x is a pointer variable
           char* y; //etc ...
           auto* z; // etc ...
           Demo* d; // etc ...
   
 
    II) If the * symbol is not preceeded by a data  
        type  AND it is followed by a pointer 
        variable (that has already been declared), 
        then we are dereferencing that pointer variable 

         Eg.   *y; //dereference y (if y is a pointer)
              cout << *z; // etc ...
               *x = 25; // etc ... 


   III)  If not (I)  or (II) AND we have two operands,
          (one on either side of the *) then the * means
          to  multiply

          Eg.
              5 * 7;  //multiply 
             
           float y = 12.10;
           2 * y; //we are multiplying              

   

*/	
	#include <iostream>


 using namespace std;



int main()
{
	int var1 = 45;
	char var2 = 'A';
	
	cout<<endl;
	cout<<"var1 = "<<var1<<endl;
	
	cout<<"var2 = '"<<var2<<"'"<<endl;
	
	cout<<endl<<endl<<endl;
	
	
	
	
	cout<<"The address of var1 = "<<&var1<<endl;
	
	 int* ptr0 ; //uninitialized pointer (DANGEROUS)
	 int* ptr1 = nullptr;
	 cout<<"ptr0 = "<<ptr0<<endl;
	 cout<<"ptr0 is pointing to "<<*ptr0<<endl;
	 cout<<"ptr1 = "<<ptr1<<endl;
	 //cout<<"ptr1 is pointing to the data  "<<*ptr1<<endl;
	 //crashes program
      
 
       int* ptr2 = &var1;
    cout<<"\n\nThe address of var1 = "<<&var1<<endl;
 	cout<<"The value of ptr2 is "<<ptr2<<endl;
 	cout<<"ptr2 is pointing to the data "<<*ptr2<<endl;
 	cout<<"The address of ptr2 is "<<&ptr2<<endl;
 	   
        ptr1 = ptr2;
        *ptr1 = 30;
	
 	cout<<"\n\nThe value of ptr1 is "<<ptr1<<endl;
 	cout<<"ptr1 is pointing to the data "<<*ptr1<<endl;
 	cout<<"The value of ptr2 is "<<ptr2<<endl;
 	cout<<"ptr2 is pointing to the data "<<*ptr2<<endl;


        int* ptr3 = new int{17};
        //do something cool with ptr3

        delete ptr3;//deallocate memory that ptr3 points to
	return 0;
	
}


/*
 

*/
